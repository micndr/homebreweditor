#Manuale d'uso.

##Index HTML.
Il caricamento dei file prende i link dei vari file scritti negli altri linguaggi di programmazione, che ne specificano varie caratteristiche, che sono il file style.css e script.js per i file relativi allo stile (definito in CSS e chiamati nella sezione head del file HTML) e per le funzioni contenute, (definito in javascript e chiamate in fondo al body): altri link utili sono il semantic.css (per lo sfruttamento degli stili sulle produzioni grafiche del framework Semantic UI), semantic.js (per lo sfruttamento delle funzioni definite nello stesso framework) e della HomeBrewLib (libreria in cui sono presenti tutte le funzioni di modifica dei dati per quel che riguarda la formulazione e l'elaborazione delle ricette delle varie birre).

### div "principale".
Area dell'editor in cui sono presenti le componenti grafiche delle attività e dei flussi: è predisposta di default all'uso di una riga, detta riga 0, che permette all'utente di cominciare a creare il modello grafico della ricetta.

#### Struttura del div "principale".
all'interno del div è presente un ulteriore div che prende il nome di "riga0" (identificato dalla classe "riga", nella cui classe vengono inseriti tutti i flussi e le attività): nel file inoltre si predispone subito il primo flusso caratterizzato dall'id "spazfre+n" (dove n rappresenta il numero di freccia): all'evento "onmouseover" viene richiamata la funzione compamenu(n), la quale mostra il menù contestuale delle opzioni eseguibili per ogni flusso considerato: tali funzioni sono "Aggiungi", "Dividi" e "modifica" (per ulteriori dettagli consultare la sezione javascript di questo readme), mentre all'evento "onmouseleave" viene richiamata la funzione "scompamenu(n, num)", che ha il compito di far scomparire lo stesso menù reso visibile dalla funzione compamenu.
all'interno del div appena definito, è presente effettivamente la rappresentazione del primo flusso, effettuata tramite l'utilizzo di un'immagine statica: "freccia0.png" contenuta nella cartella "simboli", cui è associato un id ("freccia0"), e a cui è associata all'evento "onclick" la funzione "compapara(int n, boolean freccia, int combinazione_riga_flusso, int numero_di_riga)".
il div contenuto nel div "spazfre+n" presente all'interno del div principale determina le caratteristiche che deve avere il menù contestuale del flusso: esso prende il nome di " id= "primomenu0" " con classe "ui secondary vertical menu menuint" dove l'ultima è stata definita nel file style.css mentre le altre sono definite nel file relativo alla libreria di semantic: inizialmente tale div non deve essere visibile (e prende quindi la proprietà di stile "display:none") ma viene reso visualizzabile dalla funzione "compamenu", precedentemente definita: all'interno di tale menù ci sono 3 ulteriori div:
1. Aggiungi: in tale div sono presenti 2 eventi ossia "onmouseover" e "onmouseout", che rispettivamente fanno comparire e scomparire il menù interno usando le funzioni "mostramenuint(int n)" e "chiudimenuint(int n)", ed è suddiviso in un div in funzione delle azioni eseguibili nel processo di produzione della birra, mediante le azioni di "Ammostamento" definita come azione "1", Bollitura "2", Fermentazione "3", Imbottigliamento "4": in tale div inoltre è presente il link alle varie funzioni javascript che eseguono la funzione "mettistep(int numero_dell_azione, int numero_riga, boolean split, int numero_operazione)" e questi link risultano possedere la classe item che fa riferimento a una classe presente nel framework Semantic UI;
2. Modifica: tale div richiama all'evento onclick la funzione compapara, definita anche per l'immagine del flusso;
3. Dividi: è un link che richiama la funzione "split" (successivamente spiegata nella sezione javascript).

### div "pippo"
tale div si presenta inizialmente come div vuoto e predispone lo spazio per la stampa delle informazioni relative a flussi e attività.


## Script Javascript.
tale file contiene tutte le funzioni inerenti all'editor di ricette: tali funzioni inficiano direttamente sia sulla grafica dell'editor sia sui dati in esso contenuti.

### Variabili universali.
si definiscono in partenza i vari ingredienti che sono supportati dall'editor: malti, luppoli, lieviti, acque, zuccheri e spezie, presenti sottoforma di Array.
inoltre sono presenti anche altre variabili globali:
* r: indica una variabile pronta a supportare la HBL.
* operazioni (array): tale array contiene al suo interno una serie di altri array riferiti alle righe i quali contengono ognuno tutte le operazioni svolte su quella riga.
* contatori (array): tale array contiene il numero delle operazioni svolte all'interno di una singola riga.
* numerorighe: conta il numero delle righe create.
* ricette (array): contiene il riferimento a tutte le ricette create in una singola istanza dell'editor.
* stepeseguiti: conta il numero totale di step eseguiti nell'istanza (quindi tiene conto del numero totale di flussi).
* deci (costante) = 1000 : verrà utilizzata per differenziare numericamente flussi e attività (flusso 0 porta all'attività 1000).


### $Document.ready(function).
funzione predefinita di JQuery che esegue le operazioni definite all'interno della funzione al termine del caricamento del file HTML: qui vengono inizializzati gli spazi per la prima riga: in particolare viene creata la cella 0 di "operazioni", che rappresenta di conseguenza la prima cella dell'array; vengono inizializzate alcune variabili (le prime celle di contatori e operazioni) con valore 0; viene inizializzata la variabile r associandola prima alla HBLib e poi definendo una nuova ricetta, quindi un oggetto di tipo Recipe, e inserendola dentro l'array ricette.
viene effettuato un reset per impostare tutti i valori della ricetta a 0: e viene inoltre chiamata la funzione "nuovopara(numero_dello_step, identificativo_dell_attività, numero_di_riga)" (nota bene: per i flussi il secondo parametro è sempre = 0, in quanto non attività).

### Funzione compamenu(numero_di_step_eseguiti).
tale funzione mostra a schermo il primomenù associato al numero di step: tale menù è quello che mostra a schermo le opzioni di aggiunta, modifica e split nei vari flussi e attività.

### Funzione mostramenuint(numero_di_step_eseguiti).
tale funzione mostra a schermo il primomenù associato al numero di step: tale menù è quello che mostra a schermo le opzioni di ammostamento, bollitura, fermentazione e Imbottigliamento nei vari flussi e attività.

### Funzione creaStep(numero_di_riga).
viene creato un nuovo elemento di tipo div che ha come attributi class "def azione" (con riferimento a style.css) mentre come id "stepdef" con numero di riga(convertito in stringa) e con il contatore delle operazioni su quella riga convertito in stringa; si rende poi principale il div che contiene l'elemento riga con il suo numero riga passato e gli si aggiunge il div appena creato mediante una append.

### Funzione mettistep(identificativo_azione, numero_della_riga, tipo_di_freccia, numero_di_step_associato_al_flusso).
si divide la spiegazione di tale funzione in tre fasi:
#### fase 1:
##### modifica del flusso chiamante.
in tale fase vengono create e inizializzate 6 variaibili:
* alt: indica l'altezza che deve avere l'immagine del flusso.
* nome: del file del flusso che deve essere inizializzato, ossia per esempio dei differenti tipi di freccia che sono presenti nella cartella simboli.
* mui: di tipo booleano, indica se bisogna modificare l'altezza del div che contiene il flusso: ossia indica se deve istanziare nuovo spazio per mettere un tipo diverso di freccia, più alto di quella utilizzata precedentemente.
* ind: "equazione" che viene usata per gestire completamente i valori dello z-index, ossia quelli che indicano la profondità dell'elemento nella pagina: tale valore viene usato per indicare se due elementi effettuano o meno una sovrapposizione grafica.
* spl: contenente il codice per la funzione di split.
* spazi: contiene dei breakline per separare l'immagine del flusso dal suo menù contestuale.
dopo l'inizializzazione di tali variabili vengono effettuati alcuni controlli:
* se split è pari a 0 e se si è al primo step: viene assegnato alla variabile il nome "freccia0", altrimenti, se lo step non è pari a 0 viene assegnato il valore freccia.
* se lo split è pari a 1: il nome è "split1", inoltre la variabile alt assegna l'altezza a 117px e viene resettata la variabile spl, impostata mui a 1, ind a 9 e vengono aggiunte alcune breakline alla variabile spazio.
* se lo split è pari a 2:  il nome è "split2", viene rinizializzata la variabile split.
* se split è pari a 3: il nome è "split3", alt viene settata a 117 px, spl viene resettato, e vengono aggiunte breakline nella variabile spazio.

a questo punto quello che si fa è modificare il codice HTML riferito al div con id "spazfre+step" con il codice modificato attraverso le variabili assegnate finora in questa funzione: inoltre vengono modificate alcune delle proprietà del div, nella fattispecie vengono cambiati i riferimenti alle azioni "onmouseleave" e "onmouseover", che chiamano rispettivamente le funzioni "scomparemenu2" e "compamenu2" (la differenza risiede nel fatto che sono chiamati nuovi menù diversi da quelli precedenti).
#### fase 2:
##### creazione dell'attività.
all'inizio si esegue l'attività, mediante la funzione presente nella libreria HBLib: viene richiamata la funzione creaStep(nriga), precedentemente descritta: quindi viene dichiarata una nuova variabile valo che rappresenta l'identificativo dell'attività, come stepeseguiti + deci: a quel punto, per l'attività creata, viene assegnato il codice html che la descrive, e successivamente se ne definiscono le proprietà di larghezza, altezza, classe ed eventi che ne caratterizzano quest'attività: si predispone quindi un nuovo spazio per la stampa delle informazioni relative a flussi e attività, nella fattispecie in questo caso per stampare le informazioni dell'attività, mediante la funzione nuovopara (successivamente analizzata).
#### fase 3:
##### creazione del flusso in uscita.
si aumentano le variabili contatori e operazioni riferite alla riga, quindi viene misurato lo spazio presente in funzione della vicinanza con il bordo destro, e questo, se risulta sufficientemente basso, viene aumentato di 600px: questo comporta dei bug, nella fattispecie:
* funziona una volta sola e permette la creazione per la totalità di 10 coppie attività-flusso senza mostrare errori, poi questi risultano evidenti dal disassamento del flusso rispetto all'attività.
* una volta inoltre che si attivà l'"overflow" orizzontale, se si prova a effettuare le operazioni di split, i flussi della riga così generata non saranno allineati con quelli della riga di partenza.
quindi si aumentano gli step eseguiti, e viene reinizializzata la variabile ind, viene richiamata la funzione scompamenu(numero_riga) che fa scomparire il menù dell'attività chiamante. viene quindi creato un nuovo div per contenere il nuovo flusso in uscità dall'attività con il sistema visto in precedenza (nella fattispecie sfruttando la funzione appendChild()), in cui viene posto quindi il nuovo flusso, e in cui viene inizializzato tutto il codice cui fa riferimento questo flusso, scritto in HTML; viene poi creato uno spazio per contenere tutte le informazioni del flusso, richiamando la funzione nuovopara, facendo scomparire tutti i parametri eventualmente presenti a schermo tramite la funzione scompara().

## Funzione scompamenu(numero_identificativo_del_menu).
prende in ingresso il numero del menù e porta l'attributo display a none, rendendolo di fatto invisibile.

## Funzione compamenu2(numero_relativo_al_secondo_menu_da_far_visualizzare, booleano_per_la_modifica_dell_altezza(->mui)).
fa visualizzare il div con identificativo "secondomenu+numero"; inoltre se va modificata l'altezza, quindi il booleano vale 1, viene effettuata tale modifica.

## Funzione split(numero_del_menu_del_flusso_chiamante, numero_della_riga, booleano_per_la_verifica_di_attività_successivi_al_flusso, booleano_per_indicare_la_divisione_effettuata_dal_flusso(=0), riceve_diviso, stepfatto(deprecata)).
si analizza la funzione in tre differenti fasi:
###fase 1.
####modifica del flusso chiamante.
si aumenta il numero delle righe di 1, poi si richiamano le funzioni scomparemenu2(numero_del_flusso), scompara, per far scomparire da schermo i parametri: si inizializza la variabile ind; qui vengono effettuati controlli:
* se il flusso non è stato ancora diviso, ma sono presenti altre attività dopo il flusso preso in considerazione, si effettua una differenziazione del menù risultante, ossia, viene effettuata la funzionalità di split e il menù non presenta funzionalità aggiuntive (se non solo quella di modifica dei parametri), inoltre la freccia precendentemente presente viene cambiata e convertita con una freccia risultato ("split1.png").
* se invece il flusso si trova alla fine di un attività e non presenta attività posteriori, la funzionalità di split viene applicata direttamente su di esso e viene cambiata l'immagine della freccia con quella di "split1.png" e infine viene modificato il menù eliminando la possibilità di aggiungere un ulteriore split.
* se il flusso ha già ricevuto precedentemente uno split e ne effettua un altro, tutte le immagini degli split della sua colonna vengono spostati a sinistra di 10px.
* se si effettua uno split sul primo flusso, che è già stato splittato, allora il flusso del primo split viene sostituito dall'immagine "split3.png", e inoltre si modifica il relativo menù eliminando la funzionalità di split.
infine vengono quindi modificate le caratteristiche del div "spazfre+num" relativo al div contenente il flusso chiamato.

### fase 2.
#### creazione di una nuova riga.
vengono innanzitutto aumentati i contatori, si inizializza un nuovo array all'interno di operazioni e si assegna alla prima cella del nuovo array creato l'attività 0 (attività di mash water): si predispone quindi la richiesta all'utente della quantità di volume da splittare, mediante la funzione richiediSplit() inserita dentro un ciclo while.
viene richiamata ora la funzione split sulla ricetta chiamante e il valore di ritorno viene assegnato ad una nuova ricetta che viene inserita nell'array di ricette: dopo tale split viene richiamata la funzione aggiornaQuesto(numero_riga, ultima_posizione_dell_array_degli_stati_della_ricetta) che aggiorna graficamente i parametri del flusso che è stato diviso.
dal punto di vista grafico invece: considerando l'offset del flusso chiamante viene creato un nuovo div che conterrà la nuova riga (ricetta), e viene 
posizionata all'interno del div con id "principale", definito nel file HTML: si aumenta il numero delle attività eseguite: viene ricalcolato il valore di ind e viene aumentata l'altezza del div con id "principale" di 160px per riuscire agevolmente a contenee una nuova riga.

######N.B. viene passato ultima_posizione_dell_array_degli_stati_della_ricetta, semplicemente perchè la HBLib non supporta ancora lo split effettuato prima dell'ultimo flusso della ricetta considerata: una volta che sarà implementata bisognerà modificare tale parametro.

### fase 3
#### inizializzazione di un nuovo flusso.
in questa nuova riga, per riuscire ad allineare i flussi di split tra le due righe, si crea un div vuoto per generare l'offset tra l'inizio della riga e il flusso che ne rappresenta lo split: a questo punto, si crea un div per contenere il nuovo flusso con le relative proprietà e viene aggiunto al div della riga appena creato: definito il codice html che deve avere il flusso viene associato al div del flusso appena creato: infine richiama la funzione "nuovopara(step_eseguiti,0(-> flusso), numero_righe)".

## Funzione scompara().
setta i bordi di tutte le immagini a 0px, inoltre fa scomparire tutti i div con classe pluto (ovvero tutti i parametri di flussi e attività).

## Funzione chiudimenuint(numero_riga).
viene nascosto il menù con id "menuintint+nriga" (N.B. ogni riga se tutto è andato bene, avrà solamente un menù che permette l'aggiunta di step successivi, e quindi l'id sarà univoco).

## Funzione Scomparemenu2(numero_identificativo_del_menu).
nasconde il div con id "secondomenu+num" e modifica l'altezzadel div dell'attività.

## Funzione compapara(numero_identificativo_attività, booleano_flusso, coordinate_dell'attività_considerata(->si intende riga e attività), numero_riga)
vengono richiamate le funzioni scomparemenu2(nummero_identificativo_attività), scompamenu(numero_identificativo_attività) e scompara: se si tratta di un'attività: si converte la riga e il numero_identificativo_attività in numeri e viene sommato deci (costante numerica): viene quindi creato un bordo di 3px giallo sull'attività, corrispondente all'id "step+numeroriga(toString)+numero_identificativo_attività"; inoltre viene visualizzato a schermo l'elenco dei parametri per quell'attività: se invece si tratta di un flusso viene comunque visualizzato l'elenco dei parametri del flusso e viene creato un bordo di 3px giallo sul flusso.

## Funzione nuovopara(numero_identificativo_attività/flusso, attività, numero_riga).
si crea un nuovo div con id variabile a seconda del fatto che la funzione sia richiamata da un flusso o da un'attività:
* se richiamata da un flusso (numero_identificativo_attività>1000): allora l'id sarà pari a para + numero_identificativo_attività.
* se richiamata da un'attività, l'id sarà uguale a para+numero_riga+contatori_di_quella_riga: vengono quindi creata una nuova variabile: nuovoparametro, che dovrà contenere il codice html da mostrare a schermo.
a questo punto la funzione si divide in 2 a seconda del fatto che sia un flusso o un attività:

* nel caso d'attività: vengono definite 3 variabili, ossia risul(che è pari a num-1000, e identifica univocamente tutti gli elementi che è possibile aggiungere durante ogni attività, nella fattispecie malto, lieviti, acqua, luppoli, spezie e anche le costanti), nomepasso(che indica appunto il nome dell'attività scelta, quindi ammostamento, bollitura, fermentazione e imbottigliamento) ed infine costanti (variabile contenente codice html per le costanti, tra loro diverse a seconda di una singola attività); quindi si fa un controllo sul numero dell'attività:
#### se lo step è pari a 1.
si è nella fase di ammostamento, quindi nomepasso prende la stringa "ammostamento", e viene reinizializzato l'oggetto ricette[numero_riga].process.mash per rendere possibile l'adattamento del processo alla ricetta (la HBLib non supporta ancora tutti gli ingredienti possibili, ma solo quelli maggiormente usati): si crea infine il codice html relativo alle costanti proprie dell'ammostamento, ovvero semplicemente l'aggiunta d'acqua(per lo sparge water).
#### se lo step è pari a 2.
si è nella fase di bollitura, quindi nomepasso prende la stringa "bollitura", e viene reinizializzato l'oggetto ricette[numero_riga].process.boil per rendere possibile l'adattamento del processo alla ricetta (la HBLib non supporta ancora tutti gli ingredienti possibili, ma solo quelli maggiormente usati): si crea infine il codice html relativo alle costanti proprie della bollitura, ovvero il tempo totale di bollitura, l'eventuale acqua per diluire e l'eventuale aggiunta di zucchero per dolcificare.
#### se lo step è pari a 3.
si è nella fase di fermentazione, quindi nomepasso prende la stringa "fermentazione", e viene reinizializzato l'oggetto ricette[numero_riga].process.ferment per rendere possibile l'adattamento del processo alla ricetta (la HBLib non supporta ancora tutti gli ingredienti possibili, ma solo quelli maggiormente usati): si crea infine il codice html relativo alle costanti proprie della fermentazione, ovvero la temperatura di fermentazione.
#### se lo step è pari a 4.
si è nella fase di imbottigliamento, quindi nomepasso prende la stringa "imbottigliamento", e viene reinizializzato l'oggetto ricette[numero_riga].process.prime per rendere possibile l'adattamento del processo alla ricetta (la HBLib non supporta ancora tutti gli ingredienti possibili, ma solo quelli maggiormente usati): si crea infine il codice html relativo alle costanti proprie dell'imbottigliamento, ovvero quanto zucchero per aumentare o diminuire la presenza di gas.

dopo aver terminato tale fase si imposta nella variabile nuovoparametro il codice html comprensivo di tutti gli ingredienti aggiungibili e le costanti relative alle attività: si inserisce poi l'intero codice html nel div appena creato, e ne vengono caricati gli ingredienti definiti all'inizio del file(N.B. I il caricamento dei lieviti viene effettuato una sola volta perchè non ci sono perchè nel file non è presente più di un lievito: per modificarlo seguire l'esempio di caricamento degli altri ingredienti; N.B.II benchè siano stati creati all'inizio del file due array di nome "acque" e "zucchero", questi non vengono caricati perchè la HBLib al momento non li supporta).

* nel caso di flussi: si inserisce nel nuovoparametro il codice html che fa riferimento alle proprietà del flusso in qualsiasi momento della ricetta al momento in cui si richiama tale funzione: tale codice viene infine inserito all'interno del div appena creato.

## Funzione modPara(numero_identificativo_parametri_attività, numero_riga, numero_del_flusso_all_interno_della_riga).
ha lo scopo di trasformare i paragrafi in inputbox; vengono quindi stampati a schermo gli inputbox relativi al flusso: nella fattispecie volume, densità iniziale, finale, alcol, colore, amarezza e carbonazione.

## Funzione comPara(numero_identificativo_parametri_attività, numero_riga, numero_del_flusso_all_interno_della_riga).
trasforma gli inputbox (presi nella funzione precedente), in paragrafi stampati a schermo non modificabili, aggiorna lo stato del flusso all'interno di ricette[numero_di_riga] e infine chiama la funzione aggiornaValori, con gli stessi parametri, la quale aggiorna i parametri presenti nei flussi successivi qualora essi siano stati eseguiti.

## Funzione aggiornaValori(numero_identificativo_parametri_attività, numero_riga, numero_del_flusso_all_interno_della_riga).
Si eliminano tutti i flussi e attività successivi a quelli modificati, poi si riesegue quanto svolto prima e infine si riaggiornano i flussi.

## Funzione richiediSplit(stringa_da_visualizzare).
genera un prompt che chiede all'utente di inserire un valore di split e ne ritorna il contenuto.

## Funzione aggiornaQuesto(numero_della_riga, numero_del_flusso_dentro_la_riga).
questa funzione aggiorna graficamente il valore del flusso splittato, aggiornandone i parametri uno per volta.

## Funzione mettingrediente(numero_identificativo_parametri_attività, attività, numero_identificativo_ingrediente, numero_della_riga).
tale funzione si suddivide in 3 fasi:
### fase 1: decrittazione dello stato.
si crea la nuova variabile locale risul cui viene assegnato il valore numero_identificativo_parametri_attività-deci: quindi si converte tale numero in una stringa e si controlla la sua lunghezza; se la lunghezza è uguale a 1 (siamo nella prima riga) si mette uno 0 davanti, e infine si estrae lo stato con i valori compresi dal secondo carattere fino alla fine della stringa(N.B. qui si impone un limite di massimo 9 righe).
### fase 2: operazioni relative agli ingredienti.
(verrà spiegata la sola aggiunta di un ingrediente in quanto gli altri seguono lo stesso processo) tale fase è suddivisa in ulteriori 3 fasi:
#### creazione di un div per contenere l'ingrediente inserito con le sue proprietà;
#### allocazione nel codice html di valori presi da input form all'interno delle attività;
#### aggiornamento delle proprietà dell'ingrediente all'interno del processo scelto.
##### N.B. alcuni ingredienti vengono aggiunti, altri solo aggiornati, a seconda di quello che precisamente richiede la ricetta.
### fase 3: aggiornamento dei flussi successivi tramite richiamo della funzione aggiornaValori.

## Funzione rimuoviingredienti(attività, numero_del_ingrediente_aggiunto_dello_stesso_tipo, tipo_ingrediente, quantità_da_togliere, numero_identificativo_parametri_attività, numero_riga).
questa funzione si suddivide in 3 fasi:
### modifica delle proprietà del processo della ricetta:
a seconda dell'ingrediente e a seconda dell'attività, le proprietà dell'ingrediente vengono rese nulle.
### eliminazione grafica dell'ingrediente rimosso.
### decrittazione dello stato:
vedi la funzione precedente.
### aggiornamento dei flussi successivi:
tramite il richiamo della funzione aggiornaValori.



## N.B. attualmente l'editor è graficamente predisposto ad avere più attività uguali all'interno della stessa riga: questa cosa non è attualmente supportata dalla HBLib: esempio: se io ho due ammostamenti una seguita dall'altra, e aggiungo un malto nella prima, automaticamente sarà aggiunto anche nella seconda, appunto perché non c'è ancora modo di distinguere più attività dello stesso tipo all'interno di una singola ricetta.